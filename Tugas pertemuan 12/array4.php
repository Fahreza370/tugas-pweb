<?php
    $arrNilai = array("Mie ayam"=>9000,"Bakso"=>10000,"Pizza"=>5000,"esteh"=>2000);
    echo "Menampilkan isi array asosiatif dengan foreach : <br>";
    foreach($arrNilai as $nama=>$nilai){
        echo "Harga $nama=$nilai<br>";
    }

    reset($arrNilai);
    echo "<br>Menampilkan isi array asosiatif dengan While dan List:<br>";
    while(list($nama,$nilai)=each($arrNilai)){
        echo "Harga $nama=$nilai <br>";
    }
?>