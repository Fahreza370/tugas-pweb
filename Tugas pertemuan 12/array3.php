<style>
        body{
            font-family: sans-serif;
            font-size: 23px;
            background-color:rosybrown;
        }
</style>
<body>
    <?php
    $arrWarna = array("Red","Orange","Yellow","Green","Blue","Purple","Pink");

    echo "Menampilakan isi array dengan For : <br>";
    for($i = 0 ;$i <count($arrWarna);$i++){
        echo "Warna Pelangi <font color=$arrWarna[$i]>". $arrWarna[$i]."</font><br>";
    }
    echo "<br>Menampilkan isi array dengan FOREACH: <br>";
    foreach($arrWarna as $Warna){
        echo "Warna Pelangi <font color=$Warna>".$Warna."</font><br>";
    }
    ?>
</body>
